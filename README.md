# state_management_provider_example

## 1. Project Description

Welcome to the Hello World project demonstrating state management in Flutter using the Provider plugin! This simple Flutter application showcases the fundamental principles of state management, emphasizing the utilization of the Provider package to efficiently manage and share state across widgets.

## 2. Prerequisites

Before you begin, ensure you have met the following requirements:

- [Flutter](https://flutter.dev/docs/get-started/install) installed
- [Dart SDK](https://dart.dev/get-dart) installed
- A code editor such as [Visual Studio Code](https://code.visualstudio.com/) or [Android Studio](https://developer.android.com/studio) installed
- Add latest [provider](https://pub.dev/packages/provider/install) plugin


## Getting Started

### Clone the Repository

```
git clone https://gitlab.com/techtalk5782345/flutter_state_management_provider_example.git
```

```
cd state_management_provider_example
```

## Run the Flutter App
```
flutter run
```

## Project Structure
- ``/lib/main.dart`` : Contains the main.dart file which is the entry point of the flutter app
- ``/lib/models/`` : Contains the model class
- ``/lib/providers/``: Contains the provider class
- ``/lib/screens/`` : Contains the UI screen
- ``/lib/widget/``: Contains the custom widget
