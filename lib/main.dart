// main.dart

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management_provider_example/providers/cart_provider.dart';
import 'package:state_management_provider_example/providers/products_provider.dart';

import 'screens/cart_screen.dart'; // Import the CartScreen
import 'screens/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ProductsProvider()),
        ChangeNotifierProvider(create: (_) => CartProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'E-commerce App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: '/',
        // Set the initial route
        routes: {
          '/': (context) => HomeScreen(), // HomeScreen is the initial route
          '/cart': (context) => CartScreen(), // Add CartScreen as a route
        },
      ),
    );
  }
}
