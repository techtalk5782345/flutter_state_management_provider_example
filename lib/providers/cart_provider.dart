// providers/cart_provider.dart

import 'package:flutter/foundation.dart';

import '../models/product.dart';

class CartProvider with ChangeNotifier {
  List<Product> _cartItems = [];

  List<Product> get cartItems => [..._cartItems];

  void addToCart(Product product) {
    _cartItems.add(product);
    notifyListeners();
  }

  void deleteFromCart(Product product) {
    // Remove all occurrences of the product from the cart
    _cartItems.removeWhere((item) => item.id == product.id);
    notifyListeners();
  }

  void clearCart() {
    _cartItems.clear();
    notifyListeners();
  }
}
