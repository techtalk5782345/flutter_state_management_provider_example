import 'package:flutter/foundation.dart';
import '../models/product.dart';

class ProductsProvider with ChangeNotifier {
  List<Product> _products = [
    Product(id: '1', title: 'Product 1', price: 29.99),
    Product(id: '2', title: 'Product 2', price: 19.99),
    // Add more products
  ];

  List<Product> get products => [..._products];
}
