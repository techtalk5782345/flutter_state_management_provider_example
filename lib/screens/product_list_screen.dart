import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/products_provider.dart';
import '../widgets/product_item.dart';

class ProductListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final products = Provider.of<ProductsProvider>(context).products;

    return ListView.builder(
      itemCount: products.length,
      itemBuilder: (context, index) => ProductItem(product: products[index]),
    );
  }
}
