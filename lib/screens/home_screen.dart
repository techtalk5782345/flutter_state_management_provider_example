// screens/home_screen.dart

import 'package:flutter/material.dart';

import 'product_list_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('E-commerce App'),
        actions: [
          // Add a cart icon in the AppBar
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              // Navigate to the CartScreen when the cart icon is tapped
              Navigator.pushNamed(context, '/cart');
            },
          ),
        ],
      ),
      body: ProductListScreen(),
    );
  }
}
//
